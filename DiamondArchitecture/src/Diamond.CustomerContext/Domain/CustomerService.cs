﻿using Diamond.CustomerDbContext.ACL.Ports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.Domain
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public Customer CustomerOf(string customerId)
        {
            return _customerRepository.CustomerOf(customerId);
        }
    }
}
