﻿using Diamond.CustomerDbContext.ACL.Ports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.Domain
{
    public interface ICustomerService
    {
        Customer CustomerOf(string customerId);
    }
}
