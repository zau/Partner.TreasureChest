﻿using Diamond.Foundation.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.Domain.Exceptions
{
    public class CustomerNotFoundException : DomainException
    {
    }
}
