﻿using Diamond.CustomerDbContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.ACL.Ports.Repositories
{
    public interface ICustomerRepository
    {
        Customer CustomerOf(string customerId);
    }
}
