﻿using Diamond.CustomerDbContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.OHS.Local.AppServices
{
    public interface ICustomerAppService
    {
        CustomerResponse CustomerOf(string customerId);
    }
}
