﻿using Diamond.CustomerDbContext.Domain;
using Diamond.CustomerDbContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.CustomerDbContext.OHS.Local.AppServices
{
    public class CustomerAppService : ICustomerAppService
    {
        private readonly ICustomerService _customerService;

        public CustomerAppService(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public CustomerResponse CustomerOf(string customerId)
        {
            var customer = _customerService.CustomerOf(customerId);
            return CustomerResponse.From(customer);
        }
    }
}
