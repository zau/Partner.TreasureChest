﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Diamond.CustomerDbContext.OHS.Local.AppServices;
using Diamond.CustomerDbContext.OHS.Local.PL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Diamond.CustomerDbContext.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerAppService _customerAppService;

        public CustomerController(ICustomerAppService customerAppService)
        {
            _customerAppService = customerAppService;
        }

        [HttpGet]
        public CustomerResponse CustomerOf(string customerId)
        {
            return _customerAppService.CustomerOf(customerId);
        }
    }
}
