﻿using Diamond.NotificationContext.Domain;
using Diamond.NotificationContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.OHS.Local.AppServices
{
    public class NotificationAppService : INotificationAppService
    {
        private readonly INotificationService _notificationService;

        public NotificationAppService(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        public void NotifyToCustomer(OrderPlacedEvent orderPlacedEvent)
        {
            _notificationService.Notify(orderPlacedEvent.To());
        }
    }
}
