﻿using Diamond.NotificationContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.OHS.Local.AppServices
{
    public interface INotificationAppService
    {
        void NotifyToCustomer(OrderPlacedEvent orderPlacedEvent);
    }
}
