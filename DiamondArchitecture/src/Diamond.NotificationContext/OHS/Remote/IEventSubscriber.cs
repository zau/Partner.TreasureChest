﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diamond.NotificationContext.OHS.Remote
{
    public interface IEventSubscriber
    {
        void SubscribeEvent(string eventData);
    }
}
