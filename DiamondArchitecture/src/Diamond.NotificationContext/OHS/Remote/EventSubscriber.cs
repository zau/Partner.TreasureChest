﻿using Diamond.NotificationContext.OHS.Local.AppServices;
using Diamond.NotificationContext.OHS.Local.PL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.OHS.Remote
{
    /// <summary>
    /// 事件侦听器
    /// </summary>
    public class EventSubscriber: IEventSubscriber
    {
        private readonly INotificationAppService _notificationAppService;

        public EventSubscriber(INotificationAppService notificationAppService)
        {
            _notificationAppService = notificationAppService;
        }

        public void SubscribeEvent(string eventData)
        {
            var orderPlacedEvent = JsonConvert.DeserializeObject<OrderPlacedEvent>(eventData);
            _notificationAppService.NotifyToCustomer(orderPlacedEvent);
        }
    }
}
