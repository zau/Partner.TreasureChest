﻿using Diamond.NotificationContext.ACL.Ports.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.ACL.Ports.Clients
{
    public interface ICustomerClient
    {
        CustomerResponse CustomerOf(string customerId);
    }
}
