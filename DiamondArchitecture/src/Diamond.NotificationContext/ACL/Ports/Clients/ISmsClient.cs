﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.ACL.Ports.Clients
{
    public interface ISmsClient
    {
        void Send(string phoneNumber, string message);
    }
}
