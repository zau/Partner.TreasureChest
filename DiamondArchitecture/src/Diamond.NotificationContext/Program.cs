﻿using System;
using Diamond.NotificationContext.ACL.Adapters.Clients;
using Diamond.NotificationContext.ACL.Ports.Clients;
using Diamond.NotificationContext.Domain;
using Diamond.NotificationContext.OHS.Local.AppServices;
using Diamond.NotificationContext.OHS.Remote;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Diamond.NotificationContext
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        static IHostBuilder CreateHostBuilder(string[] args)
        {
            var build = new HostBuilder();
            build.ConfigureServices((hostContext, services) =>
            {
                services.AddTransient<INotificationAppService, NotificationAppService>();
                services.AddTransient<INotificationService, NotificationService>();
                services.AddTransient<ICustomerClient, CustomerClientAdapter>();
                services.AddTransient<ISmsClient, SmsClientAdapter>();

                services.AddSingleton<IEventSubscriber, EventSubscriber>();
            });

            return build;
        }
    }
}
