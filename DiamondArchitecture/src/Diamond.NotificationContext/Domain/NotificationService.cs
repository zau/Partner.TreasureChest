﻿using Diamond.NotificationContext.ACL.Ports.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.Domain
{
    public class NotificationService : INotificationService
    {
        private readonly ICustomerClient _customerClient;
        private readonly ISmsClient _smsClient;

        public NotificationService(ICustomerClient customerClient, ISmsClient smsClient)
        {
            _customerClient = customerClient;
            _smsClient = smsClient;
        }

        public void Notify(Notification notification)
        {
            var customerResponse = _customerClient.CustomerOf(notification.To().Id);
            notification.FilledWith(customerResponse.To());
            _smsClient.Send(notification.To().PhoneNumber, notification.Content);
        }
    }
}
