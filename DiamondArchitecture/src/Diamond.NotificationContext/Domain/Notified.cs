﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.Domain
{
    public class Notified
    {
        public string Id { get; private set; }
        public string PhoneNumber { get; private set; }
    }
}
