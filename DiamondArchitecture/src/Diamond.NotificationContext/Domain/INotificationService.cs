﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.NotificationContext.Domain
{
    public interface INotificationService
    {
        void Notify(Notification notification);
    }
}
