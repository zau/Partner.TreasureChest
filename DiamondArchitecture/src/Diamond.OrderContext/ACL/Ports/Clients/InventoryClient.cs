﻿using Diamond.OrderContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.ACL.Ports.Clients
{
    public interface IInventoryClient
    {
        Task<InventoryReview> Check(Order order);

        Task Lock(Order order);
    }
}
