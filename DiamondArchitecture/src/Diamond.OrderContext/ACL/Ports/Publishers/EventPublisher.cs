﻿using Diamond.Foundation.Models;

namespace Diamond.OrderContext.ACL.Ports.Publishers
{
    public interface EventPublisher
    {
        void Publish(ApplicationEvent applicationEvent);
    }
}
