﻿using Diamond.OrderContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.ACL.Ports.Repositories
{
    public interface IOrderRepository
    {
        void Add(Order order);
    }
}
