﻿using Diamond.OrderContext.ACL.Ports.Clients;
using Diamond.OrderContext.ACL.Ports.PL;
using Diamond.OrderContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Diamond.OrderContext.ACL.Adapters.Clients
{
    public class InventoryClientAdapter : IInventoryClient
    {
        private readonly IHttpClientFactory _httpClient;
        public InventoryClientAdapter(IHttpClientFactory httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<InventoryReview> Check(Order order)
        {
            CheckingInventoryRequest request = CheckingInventoryRequest.From(order);
            var httpResponseMessage = await _httpClient.CreateClient().PostAsJsonAsync("Url", request);
            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();

            InventoryReviewResponse reviewResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<InventoryReviewResponse>(responseBody);
            return reviewResponse.To();
        }

        public async Task Lock(Order order)
        {
            LockingInventoryRequest inventoryRequest = LockingInventoryRequest.From(order);
            await _httpClient.CreateClient().PutAsJsonAsync("Url", inventoryRequest);
        }
    }
}
