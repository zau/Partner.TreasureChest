﻿using Diamond.OrderContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.OHS.Local.PL
{
    [Serializable]
    public class PlacingOrderRequest
    {
        public Order To()
        {
            return new Order();
        }
    }
}
