﻿using Diamond.OrderContext.ACL.Ports.Publishers;
using Diamond.OrderContext.Domain;
using Diamond.OrderContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.OHS.Local.AppServices
{
    public class OrderAppService : IOrderAppService
    {
        private readonly IOrderService _orderService;
        private readonly EventPublisher _eventPublisher;

        public OrderAppService(IOrderService orderService, EventPublisher eventPublisher)
        {
            _orderService = orderService;
            _eventPublisher = eventPublisher;
        }

        public async Task PlaceOrder(PlacingOrderRequest request)
        {
            var order = request.To();
            await _orderService.PlaceOrder(order);

            var orderPlaced = OrderPlacedEvent.From(order);
            _eventPublisher.Publish(orderPlaced);
        }
    }
}
