﻿using Diamond.OrderContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.OHS.Local.AppServices
{
    public interface IOrderAppService
    {
        Task PlaceOrder(PlacingOrderRequest request);
     }
}
