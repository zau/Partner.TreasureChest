﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Diamond.OrderContext.OHS.Local.AppServices;
using Diamond.OrderContext.OHS.Local.PL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Diamond.OrderContext.OHS.Remote
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderAppService _orderAppService;

        public OrderController(IOrderAppService orderAppService)
        {
            _orderAppService = orderAppService;
        }

        [HttpPost]
        public async Task PlaceOrder(PlacingOrderRequest request)
        {
            await _orderAppService.PlaceOrder(request);
        }
    }
}
