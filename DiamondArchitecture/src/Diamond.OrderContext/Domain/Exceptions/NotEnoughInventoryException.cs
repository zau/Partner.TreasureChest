﻿using Diamond.Foundation.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.OrderContext.Domain.Exceptions
{
    public class NotEnoughInventoryException : DomainException
    {
    }
}
