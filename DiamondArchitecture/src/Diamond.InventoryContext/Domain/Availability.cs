﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.Domain
{
    public class Availability
    {
        public string ProductId { get; private set; }
        public bool IsAvailable { get; private set; }

        public Availability(string productId, bool isAvailable)
        {
            ProductId = productId;
            IsAvailable = isAvailable;
        }
    }
}
