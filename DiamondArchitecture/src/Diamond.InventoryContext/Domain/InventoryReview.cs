﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.Domain
{
    public class InventoryReview
    {
        public List<Availability> Availablities { get; private set; }

        public InventoryReview(List<Availability> availablities)
        {
            if (availablities == null)
            {
                availablities = new List<Availability>();
            }

            Availablities = availablities;
        }
    }
}
