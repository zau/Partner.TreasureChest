﻿using Diamond.InventoryContext.ACL.Ports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.Domain
{
    public class InventoryService: IInventoryService
    {
        private readonly IInventoryRepository _inventoryRepository;

        public InventoryService(IInventoryRepository inventoryRepository)
        {
            _inventoryRepository = inventoryRepository;
        }

        public InventoryReview ReviewInventory(List<PurchasedProduct> purchasedProducts)
        {
            List<string> productIds = purchasedProducts.Select(p => p.ProductId).ToList();
            List<Product> products = _inventoryRepository.ProductsOf(productIds);
            List<Availability> availabilities = products.Select(p => p.CheckAvailability(purchasedProducts)).ToList();
            return new InventoryReview(availabilities);
        }
    }
}
