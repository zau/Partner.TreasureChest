﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.Domain
{
    public class PurchasedProduct
    {
        public string ProductId { get; private set; }
        public int PurchasedQuantity { get; private set; }
        public int InventoryQuantity { get; private set; }

        public PurchasedProduct(string productId, int purchasedQuantity, int inventoryQuantity)
        {
            ProductId = productId;
            PurchasedQuantity = purchasedQuantity;
            InventoryQuantity = inventoryQuantity;
        }
    }
}
