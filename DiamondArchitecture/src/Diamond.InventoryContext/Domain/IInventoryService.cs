﻿using Diamond.InventoryContext.ACL.Ports.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.Domain
{
    public interface IInventoryService
    {
        InventoryReview ReviewInventory(List<PurchasedProduct> purchasedProducts);
    }
}
