﻿using Diamond.InventoryContext.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.ACL.Ports.Repositories
{
    public interface IInventoryRepository
    {
        List<Product> ProductsOf(List<string> productIds);
    }
}
