﻿using Diamond.InventoryContext.Domain;
using Diamond.InventoryContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.OHS.Local.AppServices
{
    public class InventoryAppService : IInventoryAppService
    {
        private readonly IInventoryService _inventoryService;

        public InventoryAppService(IInventoryService inventoryService)
        {
            _inventoryService = inventoryService;
        }

        public InventoryReviewResponse CheckInventory(CheckingInventoryRequest request)
        {
            var inventoryReview = _inventoryService.ReviewInventory(request.To());
            return InventoryReviewResponse.From(inventoryReview);
        }
    }
}
