﻿using Diamond.InventoryContext.OHS.Local.PL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Diamond.InventoryContext.OHS.Local.AppServices
{
    public interface IInventoryAppService
    {
        InventoryReviewResponse CheckInventory(CheckingInventoryRequest request);
    }
}
