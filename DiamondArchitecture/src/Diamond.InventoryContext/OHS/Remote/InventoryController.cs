﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Diamond.InventoryContext.OHS.Local.AppServices;
using Diamond.InventoryContext.OHS.Local.PL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Diamond.InventoryContext.OHS.Remote
{
    [ApiController]
    [Route("[controller]")]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryAppService _inventoryAppService;

        public InventoryController(IInventoryAppService inventoryAppService)
        {
            _inventoryAppService = inventoryAppService;
        }

        [HttpPost]
        public InventoryReviewResponse Check(CheckingInventoryRequest request)
        {
            var reviewResponse = _inventoryAppService.CheckInventory(request);
            return reviewResponse;
        }
    }
}
