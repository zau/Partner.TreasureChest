﻿using Diamond.Foundation.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Diamond.Foundation.Models
{
    public class ApplicationEvent : IEvent
    {
        protected string EventId;
        protected string CreatedTimestamp;
        protected string Version;

        public ApplicationEvent()
        {
            EventId = Guid.NewGuid().ToString();
            CreatedTimestamp = DateTime.Now.ToString();
            Version = "v1.0";
        }

        public ApplicationEvent(string version)
        {
            EventId = Guid.NewGuid().ToString();
            CreatedTimestamp = DateTime.Now.ToString();
            Version = version;
        }

        string IEvent.EventId()
        {
            return EventId;
        }
    }
}
