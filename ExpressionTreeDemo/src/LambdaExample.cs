﻿using System;

namespace ExpressionTreeDemo
{
    public class LambdaExample
    {
        public delegate void CustomDelegate(string param);

        public void Test()
        {
            //{
            //    //.Net Framework 1.0/1.1
            //    CustomDelegate customDelegate = new CustomDelegate(DoWork);
            //    customDelegate.Invoke("test");
            //}
            //{
            //    //.Net Framework 2.0
            //    CustomDelegate customDelegate = new CustomDelegate(delegate (string param)
            //    {
            //        Console.WriteLine(param);
            //    });
            //    customDelegate.Invoke("test");
            //}
            //{
            //    //.Net Framework 3.0-1
            //    CustomDelegate customDelegate = new CustomDelegate((string param) =>
            //    {
            //        Console.WriteLine(param);
            //    });
            //    customDelegate.Invoke("test");
            //}
            //{
            //    //.Net Framework 3.0-2
            //    CustomDelegate customDelegate = new CustomDelegate(param =>
            //    {
            //        Console.WriteLine(param);
            //    });
            //    customDelegate.Invoke("test");
            //}
            //{
            //    //.Net Framework 3.0-3
            //    CustomDelegate customDelegate = new CustomDelegate(param => Console.WriteLine(param));
            //    customDelegate.Invoke("test");
            //}
            {
                //.Net Framework 3.0-4
                CustomDelegate customDelegate = param => Console.WriteLine(param);
                customDelegate.Invoke("test");
            }
        }

        private void DoWork(string param)
        {
            Console.WriteLine(param);
        }
    }
}
