﻿using System.Collections.Generic;

namespace ExpressionTreeDemo.Models
{
    public class Order
    {
        public List<OrderItem> OrderItems { get; set; }
    }
}
