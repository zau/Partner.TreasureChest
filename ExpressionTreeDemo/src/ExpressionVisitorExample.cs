﻿using System;
using System.Linq.Expressions;
using ExpressionTreeDemo.ExpressionVisitors;
using ExpressionTreeDemo.Models;

namespace ExpressionTreeDemo
{
    public class ExpressionVisitorExample
    {
        public void Test()
        {
            Expression<Func<int, int>> lambda = x => (x + 1) * (x - 2);
            OperationsVisitor vistor = new OperationsVisitor();
            vistor.Visit(lambda);
        }

        public void TestComplex()
        {
            Expression<Func<OrderItem, bool>> lambdaExpression = x =>
                    x.Quantity > 5 && x.Quantity < 10 &&
                    x.Name.StartsWith("a") &&
                    x.Name.EndsWith("b") &&
                    x.Name.Contains("c");

            ConditionBuilderVisitor vistor = new ConditionBuilderVisitor(lambdaExpression);
        }
    }
}
