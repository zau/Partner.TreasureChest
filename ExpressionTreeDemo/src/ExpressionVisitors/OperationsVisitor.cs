﻿using System;
using System.Linq.Expressions;

namespace ExpressionTreeDemo.ExpressionVisitors
{
    public class OperationsVisitor : ExpressionVisitor
    {
        public override Expression Visit(Expression node)
        {
            Console.WriteLine($"Visit:{node.ToString()}");
            return base.Visit(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node == null) throw new ArgumentNullException("BinaryExpression");

            if(node.NodeType == ExpressionType.Multiply)
            {
                base.Visit(node.Right);
                base.Visit(node.Left);
                return node;
            }

            if (node.NodeType == ExpressionType.Divide)
            {
                base.Visit(node.Right);
                base.Visit(node.Left);
                return node;
            }

            if (node.NodeType == ExpressionType.Add)
            {
                base.Visit(node.Right);
                base.Visit(node.Left);
                return node;
            }

            if (node.NodeType == ExpressionType.Subtract)
            {
                base.Visit(node.Right);
                base.Visit(node.Left);
                return node;
            }

            base.Visit(node.Right);
            base.Visit(node.Left);

            return node;
        }
    }
}
