﻿using System;

namespace ExpressionTreeDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //new LambdaExample().Test();
            //new ExpressionTreeExample().TestComplex();
            //new ExpressionVisitorExample().Test();
            new ExpressionVisitorExample().TestComplex();
            Console.ReadLine();
        }
    }
}
