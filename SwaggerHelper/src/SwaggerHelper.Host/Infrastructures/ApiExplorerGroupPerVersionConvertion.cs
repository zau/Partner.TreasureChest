﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Linq;

namespace SwaggerHelper.WebApi.Infrastructures
{
    /// <summary>
    /// 控制器层面分组
    /// </summary>
    public class ApiExplorerGroupPerVersionConvertion : IControllerModelConvention
    {
        public void Apply(ControllerModel controller)
        {
            var controllerNamespace = controller.ControllerType.Namespace;
            var apiVersion = controllerNamespace.Split('.').Last().ToLower();
            controller.ApiExplorer.GroupName = apiVersion;
        }
    }
}
