﻿using System;

namespace SwaggerHelper.WebApi.Models
{
    /// <summary>
    /// 模型备注
    /// </summary>
    public class WeatherForecast
    {
        /// <summary>
        /// 当前日期
        /// </summary>
        /// <example>2020-09-01</example>
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
    }
}
