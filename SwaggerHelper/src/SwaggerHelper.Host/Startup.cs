﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SwaggerHelper.WebApi.Infrastructures;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace SwaggerHelper.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //命名空间配置分组
            //services.AddControllers(option =>
            //{
            //    option.Conventions.Add(new ApiExplorerGroupPerVersionConvertion());
            //});

            //多版本配置,Mvc本身的功能
            //services.AddApiVersioning(option =>
            //{
            //    option.AssumeDefaultVersionWhenUnspecified = true;
            //    option.DefaultApiVersion = ApiVersion.Default;
            //    option.ReportApiVersions = true;
            //});

            services.AddSwaggerGen(option =>
            {
                //定义安全方案
                option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "Please enter into field the word 'Bearer' followed by a space and the JWT value",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                //应用安全方案
                option.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                      new OpenApiSecurityScheme
                      {
                          Reference = new OpenApiReference()
                          {
                              Id = "Bearer",
                              Type = ReferenceType.SecurityScheme
                          }
                      }, Array.Empty<string>()
                    }
                });

                option.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "Swagger Helper Api",
                    Description = "Swagger Helper Api",
                    Contact = new OpenApiContact()
                    {
                        Name = "Sassassin",
                        Email = "abc@qq.com"
                    }
                });
                option.SwaggerDoc("v2", new OpenApiInfo()
                {
                    Version = "v2",
                    Title = "Swagger Helper Api",
                    Description = "Swagger Helper Api",
                    Contact = new OpenApiContact()
                    {
                        Name = "Sassassin",
                        Email = "abc@qq.com"
                    }
                });

                //option.DocInclusionPredicate((docName, apiDesc) =>
                //{
                //    //如果没有设置分组则，GroupName参数为null.
                //    return apiDesc.GroupName == docName;
                //});

                //分组过滤，docName为分组名称，apiDesc为接口描述信息
                //option.DocInclusionPredicate((docName, apiDesc) =>
                //{
                //    //可设置多种过滤条件，在当前分组下过滤apiDesc的信息。

                //    //eg:只允许分组为v1的展示
                //    // if (apiDesc.GroupName == "v1") return true;

                //    //eg:只允许有ApiVersion特性标识的且为当前分组标识下的展示
                //    var versions = apiDesc.CustomAttributes()
                //        .OfType<ApiVersionAttribute>()
                //        .SelectMany(attr => attr.Versions);
                //    return versions.Any(v => $"v{v.ToString()}" == docName);

                //    //eg:只允许RelativePath为WeatherForecastOtherGroup的展示
                //    return apiDesc.RelativePath == "WeatherForecastOtherGroup";
                //});

                //如为ApiVersion特性标识多版本时启用，其他场景下无需启用
                //option.OperationFilter<RemoveVersionParameterOperationFilter>();
                //option.DocumentFilter<SetVersionInPathDocumentFilter>();

                option.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{typeof(Startup).Assembly.GetName().Name}.xml"), true);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(u =>
            {
                u.SwaggerEndpoint("/swagger/v1/swagger.json", "Swagger Helper Api V1");
                u.SwaggerEndpoint("/swagger/v2/swagger.json", "Swagger Helper Api V2");
                u.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
