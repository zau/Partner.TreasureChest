﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwaggerHelper.WebApi.Models;

namespace SwaggerHelper.WebApi.Controllers.v2
{
    [ApiController]
    //[ApiExplorerSettings(GroupName = "v2")]
    [Route("[controller]")]
    public class WeatherForecastOtherGroupController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastOtherGroupController> _logger;

        public WeatherForecastOtherGroupController(ILogger<WeatherForecastOtherGroupController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 方法注释内容
        /// </summary>
        /// <remarks>备注信息</remarks>
        /// <param>无参数</param>
        /// <response code="200">响应成功码</response>
        /// <response code="500">响应失败码</response>
        /// <returns>返回一个天气信息数组</returns>
        [HttpGet]
        [ApiExplorerSettings(GroupName = "v2")]
        public IEnumerable<WeatherForecast> GetOtherGroup()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
