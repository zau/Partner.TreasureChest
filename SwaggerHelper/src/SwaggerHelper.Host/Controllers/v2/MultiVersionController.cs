﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SwaggerHelper.WebApi.Models;

namespace SwaggerHelper.WebApi.Controllers.v2
{
    [ApiController]
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("[controller]")]

    //[ApiVersion("2")]
    //[Route("api/v{version:apiVersion}/[controller]")]
    public class MultiVersionController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<MultiVersionController> _logger;

        public MultiVersionController(ILogger<MultiVersionController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// 方法注释内容Get-2
        /// </summary>
        /// <remarks>备注信息</remarks>
        /// <param>无参数</param>
        /// <response code="200">响应成功码</response>
        /// <response code="500">响应失败码</response>
        /// <returns>返回一个天气信息数组</returns>
        [HttpGet]
        [ApiExplorerSettings(GroupName = "v2")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        /// <summary>
        /// 方法注释内容GetV2-2
        /// </summary>
        /// <remarks>备注信息</remarks>
        /// <param>无参数</param>
        /// <response code="200">响应成功码</response>
        /// <response code="500">响应失败码</response>
        /// <returns>返回一个天气信息数组</returns>
        [HttpGet]
        [Route("V2")]
        [ApiExplorerSettings(GroupName = "v2")]
        public IEnumerable<WeatherForecast> GetV2()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
