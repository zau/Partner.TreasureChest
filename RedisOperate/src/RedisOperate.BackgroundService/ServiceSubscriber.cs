﻿using Microsoft.Extensions.Hosting;
using RedisOperate.RedisTool.Interface;
using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedisOperate.BackgroundService
{
    public class ServiceSubscriber : IHostedService
    {
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var tag = path.Split('/', '\\').Last(s => !string.IsNullOrEmpty(s));
            Console.WriteLine($"Service {tag} Running");

            await Task.Run(() =>
            {
                RedisBase.ListService.Subscribe("task", (c, message) =>
                {
                    Console.WriteLine($"subscriber{tag}：{c}-{message}");
                });
            });

            await Task.Run(() =>
            {
                RedisBase.ListService.Subscribe("plan", (c, message) =>
                {
                    Console.WriteLine($"subscriber{tag}：{c}-{message}");
                });
            });
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
