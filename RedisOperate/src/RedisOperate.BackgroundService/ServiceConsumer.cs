﻿using Microsoft.Extensions.Hosting;
using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedisOperate.BackgroundService
{
    public class ServiceConsumer : IHostedService
    {
        public Task StartAsync(CancellationToken cancellationToken)
        {
            var path = AppDomain.CurrentDomain.BaseDirectory;
            var tag = path.Split('/', '\\').Last(s => !string.IsNullOrEmpty(s));
            Console.WriteLine($"Service {tag} Running");

            using (RedisListService service = new RedisListService())
            {
                while (true)
                {
                    var result = service.ListRightPop<string>("task");
                    Thread.Sleep(5000);
                    Console.WriteLine($"这里是 {tag} 队列获取的消息 {result}");
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
