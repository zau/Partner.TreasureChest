﻿using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedisOperate.App.RedisSet
{
    /// <summary>
    /// 抽奖模型，key为奖项，value为参与人Id
    /// </summary>
    class LotteryDrawModel
    {
        public static void Run()
        {
            //去重：IP统计去重；添加好友申请；投票限制；点赞；
            //交叉并的使用
            using (RedisSetService service = new RedisSetService())
            {
                service.KeyFlush();//清理全部数据

                // prize type-userId
                service.SetAdd("FirstPrize", "userId1");
                service.SetAdd("FirstPrize", "userId2");
                service.SetAdd("FirstPrize", "userId3");
                service.SetAdd("FirstPrize", "userId4");
                service.SetAdd("FirstPrize", "userId5");
                service.SetAdd("FirstPrize", "userId6");

                Console.WriteLine($"1Round：firstPrizeResult:{service.SetPop<string>("FirstPrize")}");
                Console.WriteLine($"2Round：firstPrizeResult:{service.SetPop<string>("FirstPrize")}");
                Console.WriteLine($"3Round：firstPrizeResult:{service.SetPop<string>("FirstPrize")}");

                service.SetAdd("SecondPrize", "userId2");
                service.SetAdd("SecondPrize", "userId3");
                service.SetAdd("SecondPrize", "userId6");
                service.SetAdd("SecondPrize", "userId7");
                service.SetAdd("SecondPrize", "userId8");

                Console.WriteLine($"secondPrizeResult:{service.SetPop<string>("SecondPrize")}");

                service.SetAdd("ThirdPrize", "userId2");
                service.SetAdd("ThirdPrize", "userId3");
                service.SetAdd("ThirdPrize", "userId4");
                service.SetAdd("ThirdPrize", "userId7");
                service.SetAdd("ThirdPrize", "userId9");

                Console.WriteLine($"thirdPrizeResult:{service.SetPop<string>("ThirdPrize")}");
            }
        }
    }
}
