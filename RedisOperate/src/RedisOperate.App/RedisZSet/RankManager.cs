﻿using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace RedisOperate.App.RedisZSet
{
    public class RankManager
    {
        private static readonly List<string> UserList = new List<string>()
        {
            "刘备","关羽","张飞","赵云","黄忠","马超"
        };

        public static void Run()
        {
            using (RedisZSetService service = new RedisZSetService())
            {
                service.KeyFlush();//清理全部数据

                Task.Run(() =>
                {
                    while (true)
                    {
                        foreach (var user in UserList)
                        {
                            Thread.Sleep(10);
                            service.SortedSetAdd("蜀国贡献值", user, new Random().Next(1, 100));
                        }
                        Thread.Sleep(20 * 1000);
                    }
                });

                Task.Run(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(12 * 1000);
                        Console.WriteLine("**********当前排行************");
                        int i = 1;

                        var items = service.SortedSetRangeByRankWithScores<string>("蜀国贡献值", 0, -1, true);
                        foreach (var item in items)
                        {
                            Console.WriteLine($"第{i++}名 {item.Key} 分数{item.Value}");
                        }
                    }
                });

                Console.Read();
            }
        }
    }
}
