﻿using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace RedisOperate.App.RedisList
{
    /// <summary>
    /// 博客分页数据
    /// </summary>
    public class BlogPageList
    {
        public static void Run()
        {
            using (RedisListService service = new RedisListService())
            {
                #region 初始列表
                service.ListLeftPush("blogId", "9527");
                service.ListLeftPush("blogId", "9528");
                service.ListLeftPush("blogId", "9529");
                service.ListLeftPush("blogId", "9530");
                service.ListLeftPush("blogId", "9531");
                service.ListLeftPush("blogId", "9532");
                service.ListLeftPush("blogId", "9533");
                service.ListLeftPush("blogId", "9534");
                service.ListLeftPush("blogId", "9535");
                service.ListLeftPush("blogId", "9536");
                service.ListLeftPush("blogId", "9537");
                service.ListLeftPush("blogId", "9538");
                service.ListLeftPush("blogId", "9539");
                service.ListLeftPush("blogId", "9540");
                service.ListLeftPush("blogId", "9541");
                service.ListLeftPush("blogId", "9542");
                service.ListLeftPush("blogId", "9543");
                service.ListLeftPush("blogId", "9544");
                service.ListLeftPush("blogId", "9545");
                service.ListLeftPush("blogId", "9546");
                #endregion

                #region 增加博客
                service.ListLeftPush("blogId", "9547");
                service.ListRange<string>("blogId", 0, 20);//一个list最多2的32次方-1，范围外的被移除
                #endregion

                #region 分页获取
                var page = 1;
                var size = 10;

                var blogIdsPagedResult = service.ListRange<string>("blogId", (page - 1) * size, page * size - 1);
                Console.WriteLine($"---Current Page:{page}---");
                foreach (var blogId in blogIdsPagedResult)
                {
                    Console.WriteLine(blogId);
                }
                Console.WriteLine("---Next---");

                //next page
                Console.Write("Enter:");
                Console.ReadLine();

                page++;
                var nextBlogIdsPagedResult = service.ListRange<string>("blogId", (page - 1) * size, page * size - 1);
                Console.WriteLine($"---Current Page:{page}---");
                foreach (var blogId in nextBlogIdsPagedResult)
                {
                    Console.WriteLine(blogId);
                }
                Console.WriteLine("---Next---");
                #endregion
            }
        }
    }
}
