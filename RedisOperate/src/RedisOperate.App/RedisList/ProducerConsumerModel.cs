﻿using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedisOperate.App.RedisList
{
    /// <summary>
    /// 生产者消费者模型
    /// </summary>
    public class ProducerConsumerModel
    {
        public static void Run()
        {
            using (RedisListService service = new RedisListService())
            {
                #region Seed
                service.ListLeftPush("task", "task9527");
                service.ListLeftPush("task", "task9528");
                service.ListLeftPush("task", "task9529");
                service.ListLeftPush("task", "task9530");

                service.ListRightPush("task", "task9526");
                service.ListRightPush("task", "task9525");
                service.ListRightPush("task", "task9524");
                service.ListRightPush("task", "task9523");

                var stringList = new List<string>();
                for (int i = 0; i < 10; i++)
                {
                    stringList.Add(string.Format($"newTask{i}"));
                }
                service.ListLeftPush("task", stringList);

                Console.WriteLine(service.ListLength("task"));
                #endregion

                while (true)
                {
                    Console.WriteLine("************new Task**************");
                    var newTask = Console.ReadLine();
                    service.ListLeftPush("task", newTask);
                }
            }
        }
    }
}
