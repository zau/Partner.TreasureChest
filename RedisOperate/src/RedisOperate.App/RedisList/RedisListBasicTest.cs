﻿using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedisOperate.App.RedisList
{
    /// <summary>
    /// 基础测试
    /// </summary>
    public class RedisListBasicTest
    {
        public static void Run()
        {
            using (RedisListService service = new RedisListService())
            {
                #region 初始操作
                service.KeyFlush();
                service.ListRightPush("article", "article1");
                service.ListRightPush("article", "article2");
                service.ListRightPush("article", "article3");
                service.ListRightPush("article", "article4");
                service.ListRightPush("article", "article5");
                service.ListRightPush("article", "article6");

                //可以按照添加顺序自动排序；而且可以分页获取
                var articleIds = service.ListRange<string>("article");
                var articlePagedIds = service.ListRange<string>("article", 0, 3);
                #endregion

                #region 栈模型(lpush+lpop:先进后出)
                Console.WriteLine("-----------------------------------------------");
                service.KeyFlush();
                service.ListLeftPush("article", "article1");
                service.ListLeftPush("article", "article2");
                service.ListLeftPush("article", "article3");
                service.ListLeftPush("article", "article4");
                service.ListLeftPush("article", "article5");
                service.ListLeftPush("article", "article6");

                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(service.ListLeftPop<string>("article"));

                    Console.WriteLine($"--Stack{i}--");
                    var stackResults = service.ListRange<string>("article");
                    foreach (var stackResult in stackResults)
                    {
                        Console.WriteLine(stackResult);
                    }
                }
                #endregion

                #region 队列：生产者消费者模型(lpush+rpop:先进先出)
                Console.WriteLine("-----------------------------------------------");
                service.KeyFlush();
                service.ListLeftPush("article", "article1");
                service.ListLeftPush("article", "article2");
                service.ListLeftPush("article", "article3");
                service.ListLeftPush("article", "article4");
                service.ListLeftPush("article", "article5");
                service.ListLeftPush("article", "article6");

                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(service.ListRightPop<string>("article"));

                    Console.WriteLine($"--Queue{i}--");
                    var queueResults = service.ListRange<string>("article");
                    foreach (var queueResult in queueResults)
                    {
                        Console.WriteLine(queueResult);
                    }
                }
                #endregion

                //分布式缓存，多服务器都可以访问到，多个生产者，多个消费者，任何产品只被消费一次
            }
        }
    }
}
