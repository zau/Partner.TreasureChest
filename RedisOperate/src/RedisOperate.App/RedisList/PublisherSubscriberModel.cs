﻿using RedisOperate.RedisTool.Interface;
using RedisOperate.RedisTool.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedisOperate.App.RedisList
{
    /// <summary>
    /// 发布者订阅者模型
    /// </summary>
    public class PublisherSubscriberModel
    {
        public static void Run()
        {
            using (RedisListService service = new RedisListService())
            {
                service.Publish("task", "new Task1");
                service.Publish("task", "new Task2");
                service.Publish("plan", "new Plan1");
                service.Publish("plan", "new Plan2");
            }
        }
    }
}
