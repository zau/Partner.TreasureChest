﻿using Microsoft.EntityFrameworkCore;

namespace CAPDemo.OrderService.Models
{
    public class OrderDbContext : DbContext
    {
        public OrderDbContext(DbContextOptions<OrderDbContext> options) : base(options)
        {

        }

        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().HasData(new Order
            {
                Id = 1,
                UserId = 1,
                Address = "capdemo"
            });
        }
    }
}
