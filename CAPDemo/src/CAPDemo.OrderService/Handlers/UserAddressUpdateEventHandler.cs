﻿using System.Threading.Tasks;
using CAPDemo.Events;
using CAPDemo.OrderService.Models;
using DotNetCore.CAP;
using Microsoft.EntityFrameworkCore;

namespace CAPDemo.OrderService.Handlers
{
    public class UserAddressUpdateEventHandler : IEventHandler<UserAddressUpdatedEvent>, ICapSubscribe
    {
        private readonly OrderDbContext _dbContext;

        public UserAddressUpdateEventHandler(OrderDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [CapSubscribe(nameof(UserAddressUpdatedEvent))]
        public async Task Handler(UserAddressUpdatedEvent eventData)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                var order = await _dbContext.Orders.FirstAsync(b => b.UserId == eventData.Id);
                order.Address = eventData.Address;
                await _dbContext.SaveChangesAsync();

                transaction.Commit();
            }
        }
    }
}
