using CAPDemo.UserService.Models;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CAPDemo.UserService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<UserDbContext>(options => options.UseMySql(Configuration.GetConnectionString("Default")));
            services.AddCap(x =>
            {
                x.UseEntityFramework<UserDbContext>();
                x.UseDashboard();
                x.UseRabbitMQ(rb =>
                {
                    rb.HostName = Configuration.GetValue<string>("RabbitMQ:HostName");
                    rb.Port = Configuration.GetValue<int>("RabbitMQ:Port");
                    rb.VirtualHost = Configuration.GetValue<string>("RabbitMQ:VirtualHost");
                    rb.UserName = Configuration.GetValue<string>("RabbitMQ:UserName");
                    rb.Password = Configuration.GetValue<string>("RabbitMQ:Password");
                    rb.ExchangeName = Configuration.GetValue<string>("RabbitMQ:ExchangeName");
                });

                x.SucceedMessageExpiredAfter = 24 * 3600;
                x.FailedRetryCount = 5;
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthorization();
            app.UseCapDashboard();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
