﻿namespace CAPDemo.UserService.Dtos
{
    public class UpdateUserDto
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }
}
