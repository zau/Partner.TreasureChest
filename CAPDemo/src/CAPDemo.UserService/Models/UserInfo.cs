﻿namespace CAPDemo.UserService.Models
{
    public class UserInfo
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
