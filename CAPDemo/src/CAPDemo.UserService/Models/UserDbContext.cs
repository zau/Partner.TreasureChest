﻿using Microsoft.EntityFrameworkCore;

namespace CAPDemo.UserService.Models
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {

        }

        public DbSet<UserInfo> UserInfos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserInfo>().HasData(new UserInfo
            {
                Id = 1,
                Name = "capdemo",
                Address = "chhangsha"
            });
        }
    }
}
