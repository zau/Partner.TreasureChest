﻿namespace CAPDemo.BasketService.Models
{
    public class Basket
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }
    }
}
