﻿using Microsoft.EntityFrameworkCore;

namespace CAPDemo.BasketService.Models
{
    public class BasketDbContext : DbContext
    {
        public BasketDbContext(DbContextOptions<BasketDbContext> options) : base(options)
        {

        }

        public DbSet<Basket> Baskets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Basket>().HasData(new Basket
            {
                Id = 1,
                UserId = 1,
                Address = "capdemo"
            });
        }
    }
}
