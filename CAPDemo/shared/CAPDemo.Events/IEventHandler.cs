﻿using System.Threading.Tasks;

namespace CAPDemo.Events
{
    public interface IEventHandler<T>
    {
        Task Handler(T eventData);
    }
}
