﻿namespace CAPDemo.Events
{
    public class UserAddressUpdatedEvent
    {
        public UserAddressUpdatedEvent(int id, string address)
        {
            Id = id;
            Address = address;
        }

        public int Id { get; set; }
        public string Address { get; set; }
    }
}
