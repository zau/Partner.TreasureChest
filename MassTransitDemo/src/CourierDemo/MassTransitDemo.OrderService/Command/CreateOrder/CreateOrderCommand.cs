﻿using MassTransitDemo.Shared;

namespace MassTransitDemo.OrderService.Command.CreateOrder
{
    /// <summary>
    /// 长流程 分布式事务
    /// </summary>
    public class CreateOrderCommand : IMassTransitDemoCommand
    {
        public string ProductId { get; set; }
        public string CustomerId { get; set; }
        public int Price { get; set; }
    }
}
