﻿using System;
using System.Threading.Tasks;
using MassTransit.Courier;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.OrderService.Activities.CreateOrder
{
    public class DeductBalanceActivity : IActivity<DeductBalanceModel, DeductBalanceLog>
    {
        private readonly ILogger<DeductBalanceActivity> logger;

        public DeductBalanceActivity(ILogger<DeductBalanceActivity> logger)
        {
            this.logger = logger;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<DeductBalanceModel> context)
        {
            var argument = context.Arguments;
            logger.LogInformation($"扣减余额-{DateTime.Now.ToString("HH:mm:ss")}");
            await Task.Delay(100);
            // httpClient
            return context.Completed(new DeductBalanceLog() { CustomerId = argument.CustomerId, Price = argument.Price });
        }

        public async Task<CompensationResult> Compensate(CompensateContext<DeductBalanceLog> context)
        {
            logger.LogWarning($"执行退款-{DateTime.Now.ToString("HH:mm:ss")}-￥{context.Log.Price}");
            await Task.Delay(100);
            // httpClient
            return context.Compensated();
        }
    }

    public class DeductBalanceModel
    {
        public string CustomerId { get; set; }
        public int Price { get; set; }
    }

    public class DeductBalanceLog
    {
        public string CustomerId { get; set; }
        public int Price { get; set; }
    }
}
