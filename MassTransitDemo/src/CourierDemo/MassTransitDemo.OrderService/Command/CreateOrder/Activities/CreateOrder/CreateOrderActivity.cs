﻿using System;
using System.Threading.Tasks;
using MassTransit.Courier;
using MassTransitDemo.Shared;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.OrderService.Activities.CreateOrder
{
    public class CreateOrderActivity : IExecuteActivity<CreateOrderModel>
    {
        private readonly ILogger<CreateOrderActivity> logger;

        public CreateOrderActivity(ILogger<CreateOrderActivity> logger)
        {
            this.logger = logger;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<CreateOrderModel> context)
        {
            logger.LogInformation($"创建订单-{DateTime.Now.ToString("HH:mm:ss")}");
            await Task.Delay(100);

            if (DateTime.Now.Second <= 20)
            {
                throw new CommonActivityExecuteFaildException("数据库连接超时");
            }

            if (DateTime.Now.Second > 20 && DateTime.Now.Second <= 40)
            {
                return context.FaultedWithVariables(new Exception("当日订单已达到上限"), new CreateOrderResult { OrderId = "111122", Message = "订单上限" });
            }

            return context.CompletedWithVariables(new CreateOrderResult { OrderId = "111122", Message = "创建订单成功" });
        }
    }

    public class CreateOrderModel
    {
        public string ProductId { get; set; }
        public string CustomerId { get; set; }
        public int Price { get; set; }
    }

    public class CreateOrderResult
    {
        public string OrderId { get; set; }
        public string Message { get; set; }
    }
}
