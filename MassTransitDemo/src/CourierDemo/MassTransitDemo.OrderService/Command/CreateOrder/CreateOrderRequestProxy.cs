﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier;
using MassTransitDemo.OrderService.Activities.CreateOrder;
using MassTransitDemo.OrderService.Command.CreateOrder;
using MassTransitDemo.Shared;
using Microsoft.Extensions.Configuration;

namespace Masstransit.Courier.EventualConsistency.Proxy
{
    public class CreateOrderRequestProxy : RoutingSlipDefaultRequestProxy<CreateOrderCommand>
    {
        private readonly IConfiguration configuration;

        public CreateOrderRequestProxy(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override Task BuildRoutingSlip(RoutingSlipBuilder builder, ConsumeContext<CreateOrderCommand> request)
        {
            builder.AddActivity("ReduceStock", new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/ReduceStock_execute"), new ReduceStockModel { ProductId = request.Message.ProductId });
            builder.AddActivity("DeductBalance", new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/DeductBalance_execute"), new DeductBalanceModel { CustomerId = request.Message.CustomerId, Price = request.Message.Price });
            builder.AddActivity("CreateOrder", new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/CreateOrder_execute"), new CreateOrderModel { Price = request.Message.Price, CustomerId = request.Message.CustomerId, ProductId = request.Message.ProductId });

            return Task.CompletedTask;
        }
    }
}
