﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.OrderService.Activities.CreateOrder;
using MassTransitDemo.OrderService.Command.CreateOrder;
using MassTransitDemo.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.OrderService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IRequestClient<CreateOrderCommand> _createOrderClient;
        private readonly ILogger<OrderController> _logger;

        public OrderController(
            IRequestClient<CreateOrderCommand> createOrderClient,
            ILogger<OrderController> logger)
        {
            _createOrderClient = createOrderClient;
            _logger = logger;
        }

        [HttpGet("CreateOrder")]
        public async Task<CommonCommandResponse<CreateOrderResult>> CreateOrder()
        {
            _logger.LogInformation($"发送请求-{DateTime.Now.ToString("HH:mm:ss")}");
            var result = await _createOrderClient.GetResponse<CommonCommandResponse<CreateOrderResult>>(new CreateOrderCommand()
            {
                ProductId = "11",
                Price = 100,
                CustomerId = "22"
            });

            return result.Message;
        }
    }
}
