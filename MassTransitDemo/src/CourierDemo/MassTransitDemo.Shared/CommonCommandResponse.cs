﻿namespace MassTransitDemo.Shared
{
    public class CommonCommandResponse
    {
        /// <summary>
        /// 1 成功返回 2 错误并返回错误信息 3 系统出错
        /// </summary>
        public int Status { get; set; }
        public string Message { get; set; }
    }

    public class CommonCommandResponse<TResult> : CommonCommandResponse
    {
        public TResult Result { get; set; }
    }
}
