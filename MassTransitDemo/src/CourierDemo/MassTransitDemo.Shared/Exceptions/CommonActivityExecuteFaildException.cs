﻿using System;

namespace MassTransitDemo.Shared
{
    public class CommonActivityExecuteFaildException : Exception
    {
        public CommonActivityExecuteFaildException(string message) : base(message)
        {

        }
    }
}
