﻿using System.Collections.Generic;
using GreenPipes;
using MassTransit.Courier;

namespace MassTransitDemo.Shared
{
    public class RoutingSlipCompensateErrorSpecification<TActivity, TLog> :
        IPipeSpecification<CompensateActivityContext<TActivity, TLog>>
        where TActivity : class, ICompensateActivity<TLog>
        where TLog : class
    {
        public void Apply(IPipeBuilder<CompensateActivityContext<TActivity, TLog>> builder)
        {
            builder.AddFilter(new ActivityCompensateErrorTransportFilter<TActivity, TLog>());
        }

        public IEnumerable<ValidationResult> Validate()
        {
           yield return this.Success("success");
        }
    }
}
