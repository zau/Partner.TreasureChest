﻿using System.Collections.Generic;
using GreenPipes;
using MassTransit.Courier;

namespace MassTransitDemo.Shared
{
    public class RoutingSlipExecuteErrorSpecification<TActivity, TArgument> : 
        IPipeSpecification<ExecuteActivityContext<TActivity, TArgument>>
        where TActivity : class, IExecuteActivity<TArgument>
        where TArgument : class
    {
        public void Apply(IPipeBuilder<ExecuteActivityContext<TActivity, TArgument>> builder)
        {
            builder.AddFilter(new ActivityExecuteErrorTransportFilter<TActivity, TArgument>());
        }

        public IEnumerable<ValidationResult> Validate()
        {
            yield return this.Success("success");
        }
    }
}
