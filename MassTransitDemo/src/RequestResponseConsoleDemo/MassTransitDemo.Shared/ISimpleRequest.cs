﻿using System;

namespace MassTransitDemo.Shared
{
    public interface ISimpleRequest
    {
        DateTime Timestamp { get; }
        string CustomerId { get; }
    }
}
