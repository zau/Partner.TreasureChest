﻿using MassTransitDemo.Shared;

namespace MassTransitDemo.ResponseService
{
    public class SimpleResponse : ISimpleResponse
    {
        public string CusomerName { get; set; }
    }
}
