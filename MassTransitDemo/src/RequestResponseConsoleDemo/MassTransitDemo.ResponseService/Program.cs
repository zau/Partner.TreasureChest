﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MassTransitDemo.ResponseService
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        static IHostBuilder CreateHostBuilder(string[] args)
        {
            var build = new HostBuilder();
            build.ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<ResponseHostedService>();
            });

            return build;
        }
    }
}
