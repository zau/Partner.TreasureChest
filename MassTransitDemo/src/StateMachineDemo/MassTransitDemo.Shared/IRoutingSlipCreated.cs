﻿using System;

namespace MassTransitDemo.Shared
{
    public interface IRoutingSlipCreated
    {
        /// <summary>
        /// The tracking number of the routing slip
        /// </summary>
        Guid TrackingNumber { get; }

        /// <summary>
        /// The time the routing slip was created
        /// </summary>
        DateTime Timestamp { get; }
    }
}
