﻿using System.Collections.Generic;
using MassTransit.EntityFrameworkCoreIntegration;
using MassTransit.EntityFrameworkCoreIntegration.Mappings;
using Microsoft.EntityFrameworkCore;

namespace MassTransitDemo.TrackingService.Models
{
    public class RoutingSlipStateDbContext : SagaDbContext
    {
        public RoutingSlipStateDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override IEnumerable<ISagaClassMap> Configurations
        {
            get { yield return new RoutingSlipStateSagaMap(); }
        }
    }
}
