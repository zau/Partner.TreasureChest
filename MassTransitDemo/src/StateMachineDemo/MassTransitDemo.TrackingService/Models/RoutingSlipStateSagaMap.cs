﻿using MassTransit.EntityFrameworkCoreIntegration.Mappings;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MassTransitDemo.TrackingService.Models
{
    public class RoutingSlipStateSagaMap : SagaClassMap<RoutingSlipState>
    {
        protected override void Configure(EntityTypeBuilder<RoutingSlipState> entity, ModelBuilder model)
        {
            entity.Property(x => x.CreateTime);
            entity.Property(x => x.StartTime);
            entity.Property(x => x.EndTime);
            entity.Property(x => x.Duration);
        }
    }
}
