﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier.Contracts;
using MassTransitDemo.TrackingService.Models;

namespace MassTransitDemo.TrackingService.Consumers
{
    public class RoutingSlipActivityConsumer : IConsumer<RoutingSlipActivityCompleted>
    {
        readonly string _activityName;
        readonly RoutingSlipMetrics _metrics;

        public RoutingSlipActivityConsumer(RoutingSlipMetrics metrics, string activityName)
        {
            _metrics = metrics;
            _activityName = activityName;
        }

        public Task Consume(ConsumeContext<RoutingSlipActivityCompleted> context)
        {
            Console.WriteLine($"Consume1:{context.Message.Duration}");

            if (context.Message.ActivityName.Equals(_activityName, StringComparison.OrdinalIgnoreCase))
            {
                _metrics.AddComplete(context.Message.Duration);
            }

            return Task.CompletedTask;
        }
    }
}
