﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier.Contracts;
using MassTransitDemo.TrackingService.Models;

namespace MassTransitDemo.TrackingService.Consumers
{
    public class RoutingSlipMetricsConsumer : IConsumer<RoutingSlipCompleted>
    {
        readonly RoutingSlipMetrics _metrics;

        public RoutingSlipMetricsConsumer(RoutingSlipMetrics metrics)
        {
            _metrics = metrics;
        }

        public Task Consume(ConsumeContext<RoutingSlipCompleted> context)
        {
            Console.WriteLine($"Consume:{context.Message.Duration}");
            _metrics.AddComplete(context.Message.Duration);
            return Task.CompletedTask;
        }
    }
}
