﻿using System;
using System.Threading.Tasks;
using MassTransit.Courier;
using MassTransitDemo.ProcessingService.Models;
using MassTransitDemo.Shared;

namespace MassTransitDemo.ProcessingService.Activities
{
    public class RetrieveActivity : IActivity<IRetrieveArguments, IRetrieveLog>
    {
        public async Task<ExecutionResult> Execute(ExecuteContext<IRetrieveArguments> context)
        {
            Console.WriteLine($"Address:{context.Arguments.Address}");

            if (context.Arguments.Address == 2)
            {
                await context.Publish<IContentNotFound>(new
                {
                    Timestamp = DateTime.UtcNow,
                    Address = context.Arguments.Address,
                    Reason = "error",
                });

                return await Task.FromResult(context.Completed());
            }

            return await Task.FromResult(context.Completed("completed"));
        }

        public async Task<CompensationResult> Compensate(CompensateContext<IRetrieveLog> context)
        {
            Console.WriteLine("Error");
            return await Task.FromResult(context.Compensated());
        }
    }
}
