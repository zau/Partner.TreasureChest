﻿using System;

namespace MassTransitDemo.ProcessingService.Models
{
    public interface IValidateArguments
    {
        Guid RequestId { get; }
        int Address { get; }
    }
}
