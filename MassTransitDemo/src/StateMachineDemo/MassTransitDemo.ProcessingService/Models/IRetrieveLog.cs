﻿namespace MassTransitDemo.ProcessingService.Models
{
    public interface IRetrieveLog
    {
        string LocalFilePath { get; }
    }
}
