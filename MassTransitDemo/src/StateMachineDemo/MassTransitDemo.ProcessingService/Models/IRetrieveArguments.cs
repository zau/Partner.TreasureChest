﻿using System;

namespace MassTransitDemo.ProcessingService.Models
{
    public interface IRetrieveArguments
    {
        Guid RequestId { get; }
        int Address { get; }
    }
}
