﻿using System;
using System.Threading.Tasks;
using MassTransit.Courier;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.PaymentService.Controllers
{
    public class DeductBalanceActivity : IActivity<DeductBalanceModel, DeductBalanceLog>
    {
        private readonly ILogger<DeductBalanceActivity> logger;
        private readonly PaymentDbContext _db;

        public DeductBalanceActivity(ILogger<DeductBalanceActivity> logger, PaymentDbContext db)
        {
            this.logger = logger;
            _db = db;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<DeductBalanceModel> context)
        {
            var argument = context.Arguments;
            var payment = _db.Payments.Find(argument.CustomerId);
            payment.Balance -= argument.Amount;
            _db.SaveChanges();
            logger.LogInformation($"扣减余额-{DateTime.Now.ToString("HH:mm:ss")}");
            await Task.Delay(100);
            return context.Completed(new DeductBalanceLog() { CustomerId = argument.CustomerId, Amount = argument.Amount });
        }

        public async Task<CompensationResult> Compensate(CompensateContext<DeductBalanceLog> context)
        {
            var argument = context.Log;
            var payment = _db.Payments.Find(argument.CustomerId);
            payment.Balance += argument.Amount;
            _db.SaveChanges();
            logger.LogWarning($"执行退款-{DateTime.Now.ToString("HH:mm:ss")}-￥{context.Log.Amount}");
            await Task.Delay(100);
            return context.Compensated();
        }
    }

    public class DeductBalanceModel
    {
        public string CustomerId { get; set; }
        public decimal Amount { get; set; }
    }

    public class DeductBalanceLog
    {
        public string CustomerId { get; set; }
        public decimal Amount { get; set; }
    }
}
