﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.PaymentService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var random = new Random();
            return random.Next(0, 100).ToString();
        }
    }
}
