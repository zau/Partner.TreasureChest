﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MassTransitDemo.PaymentService.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Payments",
                columns: new[] { "CustomerId", "Balance" },
                values: new object[] { "c001", 9999999m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Payments",
                keyColumn: "CustomerId",
                keyValue: "c001");
        }
    }
}
