﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace MassTransitDemo.PaymentService
{
    public class PaymentDbContext : DbContext
    {
        public DbSet<Payment> Payments { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS; Database=MassTransitDemo.PaymentService; Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Payment>().HasData(new Payment
            {
                CustomerId = "c001",
                Balance = 9999999
            });
            base.OnModelCreating(modelBuilder);
        }
    }

    public class Payment
    {
        [Key]
        [MaxLength(36)]
        public string CustomerId { get; set; }
        public decimal Balance { get; set; }
    }
}