using System;
using System.Reflection;
using MassTransit;
using MassTransitDemo.OrderService.Common.Filters;
using MassTransitDemo.PaymentService.Controllers;
using MassTransitDemo.PaymentService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace MassTransitDemo.PaymentService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PaymentDbContext>();
            services.AddEntityFrameworkSqlServer();
            services.AddControllers();
            services.AddMassTransit(x =>
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                x.AddActivities(currentAssembly);
                x.AddConsumers(currentAssembly);
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitmqConfig:HostIP"], ushort.Parse(Configuration["RabbitmqConfig:HostPort"]), Configuration["RabbitmqConfig:VirtualHost"], h =>
                    {
                        h.Username(Configuration["RabbitmqConfig:Username"]);
                        h.Password(Configuration["RabbitmqConfig:Password"]);
                    });

                    cfg.ReceiveEndpoint("DeductBalance_execute", ep =>
                    {
                        ep.ExecuteActivityHost<DeductBalanceActivity, DeductBalanceModel>(new Uri(GetServiceAddress("DeductBalance_compensate")), context);
                    });
                    cfg.ReceiveEndpoint("DeductBalance_compensate", ep =>
                    {
                        ep.CompensateActivityHost<DeductBalanceActivity, DeductBalanceLog>(context, conf =>
                        {
                            conf.AddPipeSpecification(new RoutingSlipCompensateErrorSpecification<DeductBalanceActivity, DeductBalanceLog>());
                        });
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        private string GetServiceAddress(string queueName)
        {
            return $"rabbitmq://{Configuration["RabbitmqConfig:HostIP"]}:{Configuration["RabbitmqConfig:HostPort"]}/{queueName}";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
