using System;
using System.Reflection;
using MassTransit;
using MassTransitDemo.InventoryService.Controllers;
using MassTransitDemo.OrderService.Common.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;

namespace MassTransitDemo.InventoryService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer();
            services.AddDbContext<InventoryDbContext>();
            services.AddControllers();

            services.AddMassTransit(x =>
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                x.AddActivities(currentAssembly);
                x.AddConsumers(currentAssembly);

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitmqConfig:HostIP"], ushort.Parse(Configuration["RabbitmqConfig:HostPort"]), Configuration["RabbitmqConfig:VirtualHost"], h =>
                    {
                        h.Username(Configuration["RabbitmqConfig:Username"]);
                        h.Password(Configuration["RabbitmqConfig:Password"]);
                    });

                    cfg.ReceiveEndpoint("ReduceStock_execute", ep =>
                    {
                        ep.ExecuteActivityHost<ReduceStockActivity, ReduceStockModel>(new Uri(GetServiceAddress("ReduceStock_compensate")), context);
                    });
                    cfg.ReceiveEndpoint("ReduceStock_compensate", ep =>
                    {
                        ep.CompensateActivityHost<ReduceStockActivity, ReduceStockLog>(context, conf =>
                        {
                            conf.AddPipeSpecification(new RoutingSlipCompensateErrorSpecification<ReduceStockActivity, ReduceStockLog>());
                        });
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        private string GetServiceAddress(string queueName)
        {
            return $"rabbitmq://{Configuration["RabbitmqConfig:HostIP"]}:{Configuration["RabbitmqConfig:HostPort"]}/{queueName}";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Custom seed data
            //using (var scope = app.ApplicationServices.CreateScope())
            //{
            //    var db = scope.ServiceProvider.GetService<InventoryDbContext>();
            //    if (!db.Inventories.Any())
            //    {
            //        db.Inventories.Add(new Inventory
            //        {
            //            Id = "p001",
            //            Name = "iPhone 13 Pro",
            //            InQty = 100,
            //            CurrentQty = 100
            //        });
            //        db.SaveChanges();
            //    }
            //}
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
