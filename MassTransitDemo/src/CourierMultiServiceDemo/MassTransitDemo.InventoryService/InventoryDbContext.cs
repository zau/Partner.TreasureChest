﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace MassTransitDemo.InventoryService
{
    public class InventoryDbContext : DbContext
    {
        public DbSet<Inventory> Inventories { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=localhost\SQLEXPRESS; Database=MassTransitDemo.InventoryService; Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed Data
            modelBuilder.Entity<Inventory>().HasData(new Inventory
            {
                Id = "p001",
                Name = "iPhone 13 Pro",
                InQty = 100,
                CurrentQty = 100
            });
            base.OnModelCreating(modelBuilder);
        }
    }

    public class Inventory
    {
        [Key]
        [MaxLength(36)]
        public string Id { get; set; }
        [MaxLength(200)]
        public string Name { get; set; }
        public decimal InQty { get; set; }
        public decimal OutQty { get; set; }
        public decimal CurrentQty { get; set; }
    }
}
