﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MassTransitDemo.InventoryService.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Inventories",
                columns: new[] { "Id", "CurrentQty", "InQty", "Name", "OutQty" },
                values: new object[] { "p001", 100m, 100m, "iPhone 13 Pro", 0m });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Inventories",
                keyColumn: "Id",
                keyValue: "p001");
        }
    }
}
