﻿using System;
using System.Threading.Tasks;
using MassTransit.Courier;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.InventoryService.Controllers
{
    public class ReduceStockActivity : IActivity<ReduceStockModel, ReduceStockLog>
    {
        private readonly ILogger<ReduceStockActivity> logger;
        private readonly InventoryDbContext _db;

        public ReduceStockActivity(ILogger<ReduceStockActivity> logger, InventoryDbContext db)
        {
            this.logger = logger;
            _db = db;
        }

        public async Task<ExecutionResult> Execute(ExecuteContext<ReduceStockModel> context)
        {
            var argument = context.Arguments;
            var model = _db.Inventories.Find(argument.ProductId);
            model.OutQty += argument.Qty;
            model.CurrentQty -= argument.Qty;
            _db.SaveChanges();
            logger.LogInformation($"扣减库存-{DateTime.Now.ToString("HH:mm:ss")}");
            await Task.Delay(100);
            return context.Completed(new ReduceStockLog() { ProductId = argument.ProductId, Qty = argument.Qty });
        }

        public async Task<CompensationResult> Compensate(CompensateContext<ReduceStockLog> context)
        {
            var argument = context.Log;
            var model = _db.Inventories.Find(argument.ProductId);
            model.OutQty -= argument.Qty;
            model.CurrentQty += argument.Qty;
            _db.SaveChanges();
            logger.LogWarning($"退还库存-{DateTime.Now.ToString("HH:mm:ss")}");
            await Task.Delay(100);
            return context.Compensated();
        }
    }

    public class ReduceStockModel
    {
        public string ProductId { get; set; }
        public decimal Qty { get; set; }
    }

    public class ReduceStockLog
    {
        public string ProductId { get; set; }
        public decimal Qty { get; set; }
    }
}
