﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.InventoryService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InventoryController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            var random = new Random();
            return random.Next(0, 100).ToString();
        }
    }
}
