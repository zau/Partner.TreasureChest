﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.OrderService.Commands.CreateOrder;
using MassTransitDemo.OrderService.Handlers;
using MassTransitDemo.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MassTransitDemo.OrderService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IRequestClient<CreateOrderCommand> _createOrderClient;
        private readonly ILogger<OrderController> _logger;

        public OrderController(
            IRequestClient<CreateOrderCommand> createOrderClient,
            ILogger<OrderController> logger)
        {
            _createOrderClient = createOrderClient;
            _logger = logger;
        }

        [HttpGet("CreateOrder")]
        public async Task<CommonCommandResponse<CreateOrderResult>> CreateOrder()
        {
            _logger.LogInformation($"发送请求-{DateTime.Now.ToString("HH:mm:ss")}");
            var result = await _createOrderClient.GetResponse<CommonCommandResponse<CreateOrderResult>>(new CreateOrderCommand()
            {
                ProductId = "p001",
                CustomerId = "c001",
                Qty = 1,
                Price = 7499,
                Money = 7499
            });

            return result.Message;
        }
    }
}
