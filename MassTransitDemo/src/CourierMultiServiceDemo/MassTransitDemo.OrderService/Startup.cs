using System.Reflection;
using MassTransit;
using MassTransitDemo.OrderService.Commands.CreateOrder;
using MassTransitDemo.OrderService.Handlers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MassTransitDemo.OrderService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<OrderDbContext>();
            services.AddEntityFrameworkSqlServer();
            services.AddControllers();

            services.AddMassTransit(x =>
            {
                var currentAssembly = Assembly.GetExecutingAssembly();
                x.AddActivities(currentAssembly);
                x.AddConsumers(currentAssembly);
                x.AddRequestClient<CreateOrderCommand>();
                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitmqConfig:HostIP"], ushort.Parse(Configuration["RabbitmqConfig:HostPort"]), Configuration["RabbitmqConfig:VirtualHost"], h =>
                    {
                        h.Username(Configuration["RabbitmqConfig:Username"]);
                        h.Password(Configuration["RabbitmqConfig:Password"]);
                    });

                    cfg.ReceiveEndpoint("CreateOrderCommand", ep =>
                    {
                        ep.ConfigureConsumer<CreateOrderRequestProxy>(context);
                        ep.ConfigureConsumer<CreateOrderResponseProxy>(context);
                    });

                    cfg.ReceiveEndpoint("CreateOrder_execute", ep =>
                    {
                        ep.ExecuteActivityHost<CreateOrderActivity, CreateOrderModel>(context);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        private string GetServiceAddress(string queueName)
        {
            return $"rabbitmq://{Configuration["RabbitmqConfig:HostIP"]}:{Configuration["RabbitmqConfig:HostPort"]}/{queueName}";
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
