﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier;
using MassTransitDemo.OrderService.Common.Proxy;
using MassTransitDemo.OrderService.Handlers;
using Microsoft.Extensions.Configuration;

namespace MassTransitDemo.OrderService.Commands.CreateOrder
{
    public class CreateOrderRequestProxy : RoutingSlipDefaultRequestProxy<CreateOrderCommand>
    {
        private readonly IConfiguration configuration;

        public CreateOrderRequestProxy(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        protected override Task BuildRoutingSlip(RoutingSlipBuilder builder, ConsumeContext<CreateOrderCommand> request)
        {
            builder.AddActivity("ReduceStock",
                new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/ReduceStock_execute"),
                new
                {
                    ProductId = request.Message.ProductId,
                    Qty = request.Message.Qty,
                });
            builder.AddActivity("DeductBalance",
                new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/DeductBalance_execute"),
                new
                {
                    CustomerId = request.Message.CustomerId,
                    Amount = request.Message.Money
                });
            builder.AddActivity("CreateOrder",
                new Uri($"rabbitmq://{configuration["RabbitmqConfig:HostIP"]}:{configuration["RabbitmqConfig:HostPort"]}/CreateOrder_execute"),
                new CreateOrderModel
                {
                    Qty = request.Message.Qty,
                    Price = request.Message.Price,
                    CustomerId = request.Message.CustomerId,
                    ProductId = request.Message.ProductId
                });

            return Task.CompletedTask;
        }
    }
}
