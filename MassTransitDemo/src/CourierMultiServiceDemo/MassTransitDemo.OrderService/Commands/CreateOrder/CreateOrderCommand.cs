﻿using MassTransitDemo.Shared;
using System;

namespace MassTransitDemo.OrderService.Commands.CreateOrder
{
    /// <summary>
    /// 长流程 分布式事务
    /// </summary>
    public class CreateOrderCommand : IMassTransitDemoCommand
    {
        public string ProductId { get; set; }
        public string CustomerId { get; set; }
        public decimal Qty { get; set; }
        public decimal Price { get; set; }
        public decimal Money { get; set; }
    }
}
