﻿using System;

namespace MassTransitDemo.OrderService.Common.Exceptions
{
    public class CommonActivityExecuteFaildException : Exception
    {
        public CommonActivityExecuteFaildException(string message) : base(message)
        {

        }
    }
}
