﻿namespace MassTransitDemo.Shared.Orders
{
    public interface CheckOrderStatus
    {
        string OrderId { get; }
    }
}
