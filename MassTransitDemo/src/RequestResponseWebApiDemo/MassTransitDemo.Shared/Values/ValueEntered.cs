﻿namespace MassTransitDemo.Shared
{
    public interface ValueEntered
    {
        string Value { get; }
    }
}
