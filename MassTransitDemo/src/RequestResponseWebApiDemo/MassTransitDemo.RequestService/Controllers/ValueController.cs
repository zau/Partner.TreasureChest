﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Shared;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.RequestService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ValueController : ControllerBase
    {
        readonly IPublishEndpoint _publishEndpoint;

        public ValueController(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        [HttpPost]
        public async Task<ActionResult> Post(string value)
        {
            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss")}-Publisher Value: {value}");
            await _publishEndpoint.Publish<ValueEntered>(new
            {
                Value = value
            });

            return Ok();
        }
    }
}
