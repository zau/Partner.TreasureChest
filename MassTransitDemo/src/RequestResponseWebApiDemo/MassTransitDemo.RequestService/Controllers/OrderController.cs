﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Shared.Orders;
using Microsoft.AspNetCore.Mvc;

namespace MassTransitDemo.RequestService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IRequestClient<CheckOrderStatus> _client;

        public OrderController(IRequestClient<CheckOrderStatus> client)
        {
            _client = client;
        }

        public async Task<OrderStatusResult> Get(string id)
        {
            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss")}-Request Order Id: {id}");
            var response = await _client.GetResponse<OrderStatusResult>(new { OrderId = id });
            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss")}-Response Message: {response.Message.OrderId} - {response.Message.Timestamp} - {response.Message.StatusText} - {response.Message.StatusCode}");
            return response.Message;
        }
    }
}
