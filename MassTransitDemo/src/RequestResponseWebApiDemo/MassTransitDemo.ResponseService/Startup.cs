using MassTransit;
using MassTransitDemo.ResponseService.Comsumers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MassTransitDemo.ResponseService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<CheckOrderStatusConsumer>();
                x.AddConsumer<ValueEnteredEventConsumer>();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitmqConfig:HostIP"], ushort.Parse(Configuration["RabbitmqConfig:HostPort"]), Configuration["RabbitmqConfig:VirtualHost"], h =>
                    {
                        h.Username(Configuration["RabbitmqConfig:Username"]);
                        h.Password(Configuration["RabbitmqConfig:Password"]);
                    });
                    cfg.ReceiveEndpoint("events-checkorderstatus", e =>
                    {
                        e.ConfigureConsumer<CheckOrderStatusConsumer>(context);
                    });
                    cfg.ReceiveEndpoint("events-valueentered", e =>
                    {
                        e.ConfigureConsumer<ValueEnteredEventConsumer>(context);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
