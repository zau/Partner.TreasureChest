﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Shared;

namespace MassTransitDemo.ResponseService.Comsumers
{
    public class ValueEnteredEventConsumer : IConsumer<ValueEntered>
    {
        public async Task Consume(ConsumeContext<ValueEntered> context)
        {
            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss")}-Subscriber Value: {context.Message.Value}");
            await Task.CompletedTask;
        }
    }
}
