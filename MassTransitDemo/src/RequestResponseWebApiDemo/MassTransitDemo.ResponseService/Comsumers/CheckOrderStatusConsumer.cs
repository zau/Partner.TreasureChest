﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransitDemo.Shared.Orders;

namespace MassTransitDemo.ResponseService.Comsumers
{
    public class CheckOrderStatusConsumer : IConsumer<CheckOrderStatus>
    {
        public async Task Consume(ConsumeContext<CheckOrderStatus> context)
        {
            if (context.Message.OrderId == "9527")
            {
                throw new InvalidOperationException("Order not found");
            }

            Console.WriteLine($"{DateTime.Now.ToString("hh:mm:ss")}-OrderId: {context.Message.OrderId}");

            await context.RespondAsync<OrderStatusResult>(new
            {
                OrderId = context.Message.OrderId,
                Timestamp = Guid.NewGuid().ToString(),
                StatusCode = "1",
                StatusText = "Close"
            });
        }
    }
}
