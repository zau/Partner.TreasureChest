﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Mediator;
using MassTransitDemo.MediatorService.Consumers;

namespace MassTransitDemo.MediatorService
{
    class Program
    {
        public static async Task Main()
        {
            var mediator = Bus.Factory.CreateMediator(cfg =>
            {
                cfg.Consumer<SubmitOrderConsumer>();
                cfg.Consumer<OrderStatusConsumer>();
            });

            var orderId = NewId.NextGuid();

            await mediator.Send<SubmitOrder>(new { ProductId = orderId });

            var client = mediator.CreateRequestClient<GetOrderStatus>();
            var response = await client.GetResponse<OrderStatus>(new { OrderId = orderId });
            Console.WriteLine("Order Status: {0}", response.Message.Status);
        }
    }
}
