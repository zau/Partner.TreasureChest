using System;
using MassTransit;
using MassTransitDemo.JobService.Consumers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MassTransitDemo.JobService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddMassTransit(x =>
            {
                x.AddDelayedMessageScheduler();

                x.AddConsumer<ConvertVideoJobConsumer>(typeof(ConvertVideoJobConsumerDefinition));
                x.AddConsumer<VideoConvertedConsumer>();

                x.SetKebabCaseEndpointNameFormatter();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(Configuration["RabbitmqConfig:HostIP"], Configuration["RabbitmqConfig:HostPort"], Configuration["RabbitmqConfig:VirtualHost"], h =>
                    {
                        h.Username(Configuration["RabbitmqConfig:Username"]);
                        h.Password(Configuration["RabbitmqConfig:Password"]);
                    });

                    cfg.UseDelayedMessageScheduler();
                });

                var serviceAddress = $"rabbitmq://{Configuration["RabbitmqConfig:HostIP"]}:{Configuration["RabbitmqConfig:HostPort"]}/events-checkorderstatus";
                x.AddRequestClient<ConvertVideo>(new Uri(serviceAddress));
            });
            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
