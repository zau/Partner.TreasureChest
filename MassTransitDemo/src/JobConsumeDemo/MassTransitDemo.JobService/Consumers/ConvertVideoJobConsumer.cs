﻿using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using MassTransit.JobService;

namespace MassTransitDemo.JobService.Consumers
{
    public class ConvertVideoJobConsumer : IJobConsumer<ConvertVideo>
    {
        public async Task Run(JobContext<ConvertVideo> context)
        {
            var rng = new Random();

            var variance = TimeSpan.FromMilliseconds(rng.Next(8399, 28377));

            await Task.Delay(variance);

            await context.Publish<VideoConverted>(context.Job);
        }
    }

    public class ConvertVideoJobConsumerDefinition : ConsumerDefinition<ConvertVideoJobConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator,
            IConsumerConfigurator<ConvertVideoJobConsumer> consumerConfigurator)
        {
            consumerConfigurator.Options<JobOptions<ConvertVideo>>(options =>
                options
                    .SetJobTimeout(TimeSpan.FromMinutes(10))
                    .SetConcurrentJobLimit(10));
        }
    }

    public interface ConvertVideo
    {
        string GroupId { get; }
        int Index { get; }
        int Count { get; }
        string Path { get; }
    }
}
