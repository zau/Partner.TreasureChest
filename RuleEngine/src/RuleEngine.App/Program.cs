﻿using System;
using System.Reflection;
using NRules;
using NRules.Fluent;
using RuleEngine.App.Domain;
using RuleEngine.App.Repository;
using RuleEngine.App.RuleDefine;

namespace RuleEngine.App
{
    class Program
    {
        static void Main(string[] args)
        {
            #region LoadRule From File
            //var repository = new NRules.RuleSharp.RuleRepository();
            //repository.AddNamespace("System");
            //repository.AddReference(typeof(Order).Assembly);
            //repository.Load(@"Discount.rul");
            #endregion

            #region LoadRule From Runtime
            //var repository = new CustomRuleRepository();
            //repository.LoadRules();
            #endregion

            #region LoadRule From Fluent Define
            var repository = new RuleRepository();
            repository.Load(x => x.From(typeof(PreferredCustomerDiscountRule).Assembly));

            // Filter
            //repository.Load(x => x
            //    .From(Assembly.GetExecutingAssembly())
            //    .Where(rule => rule.Name.StartsWith("Test") || rule.IsTagged("Test"))
            //    .To("MyRuleSet"));
            #endregion

            #region LoadModel
            var sessionFactory = repository.Compile();
            var session = sessionFactory.CreateSession();
            var customer = new Customer("John Doe") { IsPreferred = true };
            var order1 = new Order(123456, customer, 2, 25.0);
            var order2 = new Order(123457, customer, 1, 100.0);

            session.Insert(customer);
            session.Insert(order1);
            session.Insert(order2);
            #endregion

            #region Run
            session.Fire();
            #endregion

            #region Query
            var customers = session.Query<Customer>();
            var orders = session.Query<Order>();
            #endregion

        }
    }
}
