﻿using System;
using System.Threading.Tasks;
using static PipeLineSimulation.App.ApplicationFactory;

namespace PipeLineSimulation.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PipeLine !");

            var applicationFactory = new ApplicationFactory();
            applicationFactory.Use(next =>
            {
                Console.WriteLine("Pre 1 Start");
                ApplicationDelegate applicationDelegate = async c =>
                {
                    Console.WriteLine("1 Start");
                    await next.Invoke(c);
                    Console.WriteLine("1 End");
                };
                return applicationDelegate;
            });
            applicationFactory.Use(next =>
            {
                Console.WriteLine("Pre 2 Start");
                ApplicationDelegate applicationDelegate = async c =>
                {
                    Console.WriteLine("2 Start");
                    await next.Invoke(c);
                    Console.WriteLine("2 End");
                };
                return applicationDelegate;
            });

            var application = applicationFactory.Build();

            application.Invoke("0");

            Console.ReadLine();
        }
    }
}
