﻿using System;
using WorkflowCore.Interface;
using WorkflowCore.Models;

namespace WorkFlowEnginee.Web.Steps
{
    public class GoodByeWorld : StepBody
    {
        public override ExecutionResult Run(IStepExecutionContext context)
        {
            Console.WriteLine("Good bye world");
            return ExecutionResult.Next();
        }
    }
}
