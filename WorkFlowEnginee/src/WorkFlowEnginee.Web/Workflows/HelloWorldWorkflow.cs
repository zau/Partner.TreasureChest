﻿using WorkflowCore.Interface;
using WorkFlowEnginee.Web.Steps;

namespace WorkFlowEnginee.Web.Workflows
{
    public class HelloWorldWorkflow : IWorkflow
    {
        public string Id => "HelloWorld";
        public int Version => 1;

        public void Build(IWorkflowBuilder<object> builder)
        {
            builder.StartWith<HelloWorld>()
                .Then<GoodByeWorld>();
        }
    }
}
