﻿using System.Collections.Generic;

namespace LayuiFrameDemo.Admin.Models
{
    public class CommonResult<T>
    {
        public int Code { get; set; }
        public string Msg { get; set; }
        public List<T> Data { get; set; }
        public int Count { get; set; }
    }
}
