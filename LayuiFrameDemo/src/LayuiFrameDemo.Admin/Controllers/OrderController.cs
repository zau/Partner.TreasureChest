﻿using System.Collections.Generic;
using LayuiFrameDemo.Admin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace LayuiFrameDemo.Admin.Controllers
{
    public class OrderController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public OrderController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public CommonResult<OrderViewModel> GetOrderList()
        {
            return new CommonResult<OrderViewModel>()
            {
                Code = 200,
                Msg = "",
                Count = 1,
                Data = new List<OrderViewModel>()
                {
                    new OrderViewModel()
                    {
                        DisplayName = "test"
                    }
                }
            };
        }

        public IActionResult CreateOrUpdate()
        {
            return View();
        }
    }
}
